Camel CXF PAYLOAD -> Queue
=========================================

CXF Payload Example inbound CustomerService lookupCustomer

Request:
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="http://demo.fusesource.com/wsdl/CustomerService/" xmlns:part="http://www.fusesource.com/demo/part">
   <soapenv:Header/>
   <soapenv:Body>
      <cus:lookupCustomer>
         <customerId>cId</customerId>
         <customerDataSet1>cds1</customerDataSet1>
         <part>
            <part:name>pName1</part:name>
            <part:description>pDesc1</part:description>
         </part>
      </cus:lookupCustomer>
   </soapenv:Body>
</soapenv:Envelope>

Response:
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns2:lookupCustomerResponse xmlns:ns2="http://demo.fusesource.com/wsdl/CustomerService/" xmlns:ns3="http://www.fusesource.com/demo/customer">
         <ret>
            <ns3:firstName>Ade</ns3:firstName>
            <ns3:lastName>Trenaman</ns3:lastName>
            <ns3:phoneNumber>012343210</ns3:phoneNumber>
            <ns3:id>cId</ns3:id>
         </ret>
      </ns2:lookupCustomerResponse>
   </soap:Body>
</soap:Envelope>


Inbound CXF Web Service
Log
Unmarshall PAYLOAD (XML text) to LookupCustomer WSDL2JAVA generated object
Call Processor
Velocity template to generate WS Response




To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache ServiceMix
or Apache Karaf. You can run the following command from its shell:

    karaf@root> features:install camel-cxf
    karaf@root> features:install camel-velocity
    osgi:install -s mvn:com.redhat.test/camel-cxf-message/1.0.0-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/
