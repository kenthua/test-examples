
package com.fusesource.demo.part;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fusesource.demo.part package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Part_QNAME = new QName("http://www.fusesource.com/demo/part", "Part");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fusesource.demo.part
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Part }
     * 
     */
    public Part createPart() {
        return new Part();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Part }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fusesource.com/demo/part", name = "Part")
    public JAXBElement<Part> createPart(Part value) {
        return new JAXBElement<Part>(_Part_QNAME, Part.class, null, value);
    }

}
