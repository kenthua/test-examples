/**
 * Copyright 2011 FuseSource
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.redhat.test.cxf.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.message.MessageContentsList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fusesource.demo.customer.Customer;
import com.fusesource.demo.part.Part;
import com.fusesource.demo.wsdl.customerservice.LookupCustomer;

public class LookupCustomerProcessor implements Processor {

    public static final Logger log = LoggerFactory.getLogger(LookupCustomerProcessor.class);

       
    public void process(Exchange exchng) throws Exception {

        // PAYLOAD
    	
    	System.out.println(">> LCP exchange body type: " + exchng.getIn().getBody().getClass());
    	LookupCustomer lc = exchng.getIn().getBody(LookupCustomer.class);
    	System.out.println(">> LCP LC: " + lc);
        System.out.println(">> LCP customerId:" + lc.getCustomerId());
        System.out.println(">> LCP customerDataSet1:" + lc.getCustomerDataSet1());
        System.out.println(">> LCP part.name:" + lc.getPart().getName());
        System.out.println(">> LCP part.description:" + lc.getPart().getDescription());  
        /*
        Customer c = new Customer();
        c.setFirstName("Ade");
        c.setLastName("Trenaman");
        c.setId(lc.getCustomerId());
        c.setPhoneNumber("+353-1-01234567");
        
        exchng.getOut().setBody(c);
        */
        
        exchng.getIn().setHeader("firstName", "Ade");
        exchng.getIn().setHeader("lastName", "Trenaman");
        exchng.getIn().setHeader("phoneNumber", "012343210");
        exchng.getIn().setHeader("id", lc.getCustomerId());

        
    	// via Object[]
    	/*
    	Object[] args = exchng.getIn().getBody(Object[].class);
        String customerId = (String) args[0];
        System.out.println(">> LCP args: " + args);
        String cds1 = (String) args[1];
        Part part = (Part) args[2];
        System.out.println(">> LCP customerId:" + customerId);
        System.out.println(">> LCP customerDataSet1:" + cds1);
        System.out.println(">> LCP part.name:" + part.getName());
        System.out.println(">> LCP part.description:" + part.getDescription());
        
        // via MessageContentsList 
        MessageContentsList mcl = (MessageContentsList) exchng.getIn().getBody();
        System.out.println(">> LCP mcl:" + mcl.toString());
        String mclCustomerId = (String) mcl.get(0);
        String mclCustomerDataSet1 = (String) mcl.get(1);
        Part mclPart = (Part) mcl.get(2);
        System.out.println(">> LCP MCL customerId:" + mclCustomerId);
        System.out.println(">> LCP MCL customerDataSet1:" + mclCustomerDataSet1);
        System.out.println(">> LCP MCL part.name:" + mclPart.getName());
        System.out.println(">> LCP MCL part.description:" + mclPart.getDescription());
        
        Customer c = new Customer();
        c.setFirstName("Ade");
        c.setLastName("Trenaman");
        c.setId(customerId);
        c.setPhoneNumber("+353-1-01234567");
        

        exchng.getOut().setBody(new Object[] {c});
        */
    }
}
